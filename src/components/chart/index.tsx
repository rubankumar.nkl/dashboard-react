import dynamic from "next/dynamic";

export const OneLineChart = dynamic(() => import("./oneLineChart"), {
    ssr: false,
});

export const BarGraphChart = dynamic(() => import("./barGraphChart"), {
    ssr: false,
});
