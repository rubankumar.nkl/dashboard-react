import React from "react";
import { Suspense } from 'react'
import { useQuery } from "react-query";

import { OneLineChart, BarGraphChart } from "@components/chart";

const API_URL = "https://api.npoint.io/215566d8103004a77d51";

export const Main: React.FC = () => {
    const [graphType, setGraphType] = React.useState("line");

    const {
        data: Graphdata,
        isLoading,
        error,
    } = useQuery("repoData", () => fetch(API_URL).then((res) => res.json()), {
        refetchOnWindowFocus: false,
    });

    const CurrentGraph = () => {
        return graphType === "line" ? (
            <OneLineChart data={Graphdata} />
        ) : (
            <BarGraphChart data={Graphdata} />
        );
    };

    const Loading = ({ value }: {value: string}) => {
        return <div className="h-[240px] flex justify-center items-center animate-pulse">{value}</div>
    };

    return (
        <main>
            <div className="flex items-center justify-end mb-8">
                <div className="flex gap-2 rounded-md p-1 bg-gray-50">
                    <button
                        onClick={() => setGraphType("line")}
                        className={`py-1.5 px-4 rounded-md ${
                            graphType === "line"
                                ? "bg-indigo-500 text-white font-medium"
                                : "text-gray-700"
                        }`}
                    >
                        Line chart
                    </button>
                    <button
                        onClick={() => setGraphType("bar")}
                        className={`py-1.5 px-4 rounded-md ${
                            graphType === "bar"
                                ? "bg-indigo-500 text-white font-medium"
                                : "text-gray-700"
                        }`}
                    >
                        Bar chart
                    </button>
                </div>
            </div>

            <div className="grid grid-cols-6 grid-rows-2 gap-12 md:gap-16">
                <div className="col-span-6 lg:col-span-3 shadow-lg rounded-lg p-4">
                    {isLoading ? (
                       <Loading value="Downloading data..."/>
                    ) : (
                        <Suspense fallback={<Loading value="Loading Graph..."/>}>
                            <CurrentGraph />
                        </Suspense>
                    )}
                </div>
                <div className="col-span-6 lg:col-span-3 shadow-lg rounded-lg p-4">
                    {isLoading ? (
                       <Loading value="Downloading data..."/>
                    ) : (
                        <Suspense fallback={<Loading value="Loading Graph..."/>}>
                            <CurrentGraph />
                        </Suspense>
                    )}
                </div>

                <div className="col-span-6 shadow-lg rounded-lg p-4">
                    {isLoading ? (
                       <Loading value="Downloading data..."/>
                    ) : (
                        <Suspense fallback={<Loading value="Loading Graph..."/>}>
                            <CurrentGraph />
                        </Suspense>
                    )}
                </div>
            </div>
        </main>
    );
};
