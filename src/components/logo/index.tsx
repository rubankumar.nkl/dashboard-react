import React from "react";
import Image from "next/image";

export const Logo: React.FC = () => {
    return (
        <Image
            data-test="icon"
            src="/icons/logo-icon.svg"
            alt="Logo icon"
            width="40"
            height="40"
        />
    );
};
